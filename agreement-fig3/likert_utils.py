def convert(q, q1, max_width_per_side = 1):

	for key in ["Strongly Agree", "Agree", "Neutral", "Disagree", "Strongly Disagree"]:
		if not key in q1:
			q1[key] = 0

	print(q1)

	left = q1["Strongly Disagree"] + q1["Disagree"] + 0.5 * q1["Neutral"]
	right = q1["Strongly Agree"] + q1["Agree"] + 0.5 * q1["Neutral"]
	total = int(left + right)

	_l = round(max_width_per_side - left/total, 3)
	sd = round(q1["Strongly Disagree"] / total, 3)
	d = round(q1["Disagree"] / total, 3)
	n = round(q1["Neutral"] / total, 3)
	a = round(q1["Agree"] / total, 3)
	sa = round(q1["Strongly Agree"] / total, 3)
	_r = round(max_width_per_side - right/total, 3)
	
	res = [q, total	, _l, sd, d, n, a, sa, _r]
	return res