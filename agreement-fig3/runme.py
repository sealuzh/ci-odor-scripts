import preprocessing
import likert_utils
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

# q1, q2
results = {
    'q1': ["SA", "SA", "SA", "A", "N", "D","SD", "No Answer"],
    'q2': ["SA", "A", "A", "N", "D", "SD", np.nan, "No Answer"]
}
df = pd.DataFrame(data=results)
df = preprocessing.readSurvey()



questions = {
    "problem_relaxed_principles": "Problem: CI is not strict",
    "problem_unintentional_deviation": "Problem: Unintentional deviation",
    "problem_ci_decay": "Problem: CI decays",
#
    "relevance_late_merging": "Late Merging: Relevance",
    "detect_late_merging": "Late Merging: Detection",
    "relevance_aged_branches": "Aged Branches: Relevance",
    "detect_aged_branches": "Aged Branches: Detection",
    "relevance_broken_master": "Broken Release Branch: Relevance",
    "detect_broken_master": "Broken Release Branch: Detection",
    "relevance_bloated_repo": "Bloated Repo: Relevance",
    "detect_bloated_repo": "Bloated Repo: Detection",
    "relevance_scheduled_builds": "Scheduled Builds: Relevance",
    "detect_scheduled_builds": "Scheduled Builds: Detection",
    "relevance_absent_feedback": "Absent Feedback: Relevance",
    "detect_absent_feedback": "Absent Feedback: Detection",
    "relevance_email_only_notifications": "Email-Only: Relevance",
    "detect_email_only_notifications": "Email-Only: Detection",
    "relevance_skip_failed_tests": "Skip Failed Tests: Relevance",
    "detect_skip_failed_tests": "Skip Failed Tests: Detection",
    "relevance_slow_builds": "Slow Build: Relevance",
    "detect_slow_builds": "Slow Build: Detection",
#
    "ciodor_would_integrate": "I would use summaries in my CI process",
#
    "": "Report-Contains useful/relevant warnings",
    "": "Report-I learned from it",
    "": "Report-It made me curious",
#
    "": "Slow build identification appropriate",
    "": "Slow build summary actionable",
    "": "Slow build should make the build fail",
#
    "": "Overall - This info is unavailable in other tools",
    "": "Overall - The reports provide a good overview",
    "": "Overall - Frequent reports have a positive effect",
    "": "Overall - I would integrate in my pipeline",
}

def to_questions(series):
    return series.map(lambda e: questions[e] if e in questions else e)

df = df[[k for k in df.keys() if k.startswith("relevance_") or k.startswith("detect_") or k.startswith("problem_") or k.startswith("ciodor_would_")]]
df = df.drop(["problem_decay_effect"], axis=1)
print(df[:10])






keys = df.keys()
qs = []

for key in keys:
    q = df.groupby(key)[key]
    q = pd.DataFrame(q.size())[key]
    if "No Answer" in q.index:
        q = q.drop("No Answer")
    q = likert_utils.convert(key, q)
    qs.append(q)

print(qs)



x = pd.DataFrame(qs, columns=["Question", "Count", "L", "SD", "D", "N", "A", "SA", "R"])
#x["Question_Long"] =
x= x.set_index(to_questions(x["Question"]))#,drop=True,inplace=True)



x = x.iloc[::-1]
print(x)

counts = x["Count"]
print(counts)

if "Question" in x.columns:
    print("Dropping column 'Question'")
    x = x.drop(["Question"], axis=1)

if "Count" in x.columns:
    print("Dropping column 'Count'")
    x = x.drop(["Count"], axis=1)

print(x)

likert_colors = ['white', 'firebrick', 'lightcoral', 'gainsboro', 'cornflowerblue', 'darkblue']

a = x.plot.barh(stacked=True, color=likert_colors, figsize=(4, 6), width=.7)
a.set_xlabel("Level of Agreement")
a.autoscale(tight=False)
a.set_xlim([0, 2])

a.figure.patch.set_facecolor('white')

# add proper legend
SD_patch = mpatches.Patch(color='firebrick', label='Strongly Disagree')
D_patch = mpatches.Patch(color='lightcoral', label='Disagree')
N_patch = mpatches.Patch(color='gainsboro', label='Neutral')
A_patch = mpatches.Patch(color='cornflowerblue', label='Agree')
SA_patch = mpatches.Patch(color='darkblue', label='Strongly Agree')
patches = [SD_patch, D_patch, N_patch, A_patch, SA_patch]
a.legend(handles=patches, loc='upper center', frameon=False, ncol=5, bbox_to_anchor=(0.2, 1.07))

delta = 0
for index, row in x.iterrows():
    y = delta - 0.02
    delta += 1

    positive = (row["SA"] + row["A"]) * 100
    neutral = row["N"] * 100
    negative = (row["D"] + row["SD"]) * 100

    a.text(0.02, y, "{:.1f}%".format(negative),
           horizontalalignment='left',
           verticalalignment='center',
           color='lightgray',
           weight='bold',
           bbox=dict(facecolor='none', edgecolor='none'))

    a.text(1, y, "{:.1f}%".format(neutral),
           horizontalalignment='center',
           verticalalignment='center',
           color='gray',
           bbox=dict(facecolor='none', edgecolor='none'))

    a.text(1.98, y, "{:.1f}%".format(positive),
           horizontalalignment='right',
           verticalalignment='center',
           color='lightgray',
           weight='bold',
           bbox=dict(facecolor='none', edgecolor='none'))

    a.text(2.02, y, "{} answers".format(counts[index]),
           horizontalalignment='left',
           verticalalignment='center', )

# a.axes.get_xaxis().set_visible(False)
a.spines['top'].set_visible(False)
a.spines['bottom'].set_visible(False)
plt.tick_params(
    axis='x',  # changes apply to the x-axis
    which='both',  # both major and minor ticks are affected
    bottom=False,  # ticks along the bottom edge are off
    top=False,  # ticks along the top edge are off
    labelbottom=False)  # labels along the bottom edge are off

z = plt.axvline(1, color='black', linewidth=1, linestyle="dotted")
z.set_zorder(-1)

plt.savefig('../agreement-fig3.pdf', bbox_inches="tight")