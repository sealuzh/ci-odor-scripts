import pandas as pd

renames = {
    # id
    "Response ID": "id",
    "Before answering this survey, have you already seen one of our reports for your own project?": "has_seen_report",
   "The report is useful for my project and contains relevant warnings.": "report_is_useful",
   "I have learned something about my project that I have not been aware of before.": "report_learned_something",
   "The results made me curious. I plan to investigate the different warnings for my project.": "report_made_me_curious",
   "Which is the name of your project analyzed in the report? (Optional)": "report_project",
   "Which is the name of your project analyzed in the report?": "report_project",
   "Do you want to share any thoughts about our report for your CI process? (Optional)": "report_thoughts",
    # slow build
   "This summary helps me to identify a slow build.": "slowbuild_identification",
   "I know how to address the different warnings about slow builds.": "slowbuild_actionable",
   "If a report contains high-severity warnings about slow builds, the build should fail.": "slowbuild_shouldfail",
    "Do you want to share any additional thoughts on the \"slow build\" anti-pattern with us? (Optional)": "slowbuild_thoughts",
    # failed tests
    "This summary helps me to identify when failed-tests are being skipped.": "failedtests_identification",
    "I know how to prevent future warnings about failed-test skipping.": "failedtests_actionable",
    "If a report contains high-severity warnings about failed-test skipping, the build should fail.": "failedtests_shouldfail",
    "Do you want to share any additional thoughts on the \"failed-test skipping\" anti-pattern with us? (Optional)": "failedtests_thoughts",
    # late merging
    "This summary helps me to identify late merging.": "latemerging_identification",
    "I know how to address the different warnings about late merging.": "latemerging_actionable",
    "If a report contains high-severity warnings about late merging, the build should fail.": "latemerging_shouldfail",
    "Do you want to share any additional thoughts on the \"late merging\" anti-pattern with us? (Optional)": "latemerging_thoughts",
    # broken release branch
    "This summary improves awareness about the frequency of release-branch failures.": "brokenmaster_awareness_freq",
    "This summary improves awareness about the time it takes to fix release-branch failures.": "brokenmaster_awareness_timetofix",
    "I know how to improve the trend of this summary in the future.": "brokenmaster_actionable",
    "Do you want to share any additional thoughts on the \"broken release branch\" anti-pattern with us? (Optional)": "brokenmaster_thoughts",
    # overall
    "The CI report provides information that is not available in any other tool.": "overall_novelty",
    "The reports provide a good overview over the CI practices of a project.": "overall_overview",
    "Having these reports frequently would have a positive influence on the CI practices of my team.": "overall_positive_effect",
    "I would like to integrate such a reporting in my own CI pipeline.": "overall_would_integrate",
    "Do you want to share any additional thoughts about the CI reports with us? (Optional)": "overall_thoughts",
   # demographics
   "What is your highest qualification related to Computer Science?": "demographics_education",
   "Other - Write In:What is your highest qualification related to Computer Science?": "demographics_education_other",
   "What is your current position?": "demographics_position",
   "Other - Write In:What is your current position?": "demographics_position_other",
   "In which domain do you typically create software (e.g., financial, game, etc.)?": "demographics_domain",
   "How would you rate your general programming experience?": "demographics_experience_programming",
   "How would you rate your knowledge about CI principles?": "demographics_ci_theory",
   "How would you rate your experience using and maintaining a CI pipeline?": "demographics_ci_practice",
   "In which role do you typically get in contact with CI (e.g., as developer, tester, product owner, etc.)?": "demographics_role",
   "What is your name? (Optional)": "name",
   "What is you email address? (Optional)": "email",
   "I would like to participate in the raffle of the two vouchers.:Raffle and follow-up questions (Optional)": "include_in_raffle",
   "You can contact me again in the context of this study, e.g., to inform me about the results or for clarification questions.:Raffle and follow-up questions (Optional)": "can_contact",
   "Do you have any further comments regarding this questionnaire or our research in general? (Optional)": "questionnaire_thoughts",
}

drops = [
    "Time Started",
    "Date Submitted",
    "Referer",
    "Extended Referer",
    "SessionID",
    "User Agent",
    "Extended User Agent",
    "Contact ID",
    "Legacy Comments",
    "Comments",
    "Tags",
    "Language",
    "IP Address",
    "Longitude",
    "Latitude",
    "Country",
    "City",
    "State/Region",
    "Postal",
]

questions = {}

for key in renames:
    questions[renames[key]] = key

def readRawSurvey(csv_file):
    print ("reading survey from " + csv_file)
    df = pd.read_csv(csv_file)
    df = df.rename(index=str, columns=renames)
    df = df.set_index('id')

    df["include_in_raffle"] = df['include_in_raffle'].notnull()
    df["can_contact"] = df['can_contact'].notnull()


    df = df.drop(drops, axis=1)

    return df

def readPreprocessedSurvey(csv_file = "../survey2-preprocessed.csv"):
    print ("reading preprocessed survey from " + csv_file)
    df = pd.read_csv(csv_file, index_col=0)
    numTotal = len(df)
    print("contains {} responses".format(numTotal))
    return df
