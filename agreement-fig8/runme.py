import preprocessing2 as pp
import likert_utils
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

# q1, q2
results = {
    'q1': ["SA", "SA", "SA", "A", "N", "D","SD", "No Answer"],
    'q2': ["SA", "A", "A", "N", "D", "SD", np.nan, "No Answer"]
}
df = pd.DataFrame(data=results)
df = pp.readPreprocessedSurvey()

df = df.drop(["report_thoughts", "report_project", "overall_thoughts"], axis=1)
#df = df[(df["has_seen_report"] == "Yes")]





questions = {
    "problem_relaxed_principles": "Problem: CI is not strict",
    "problem_unintentional_deviation": "Problem: Unintentional deviation",
    "problem_ci_decay": "Problem: CI decays",
#
    "relevance_late_merging": "Late Merging: Relevance",
    "detect_late_merging": "Late Merging: Detection",
    "relevance_aged_branches": "Aged Branches: Relevance",
    "detect_aged_branches": "Aged Branches: Detection",
    "relevance_broken_master": "Broken Release Branch: Relevance",
    "detect_broken_master": "Broken Release Branch: Detection",
    "relevance_bloated_repo": "Bloated Repo: Relevance",
    "detect_bloated_repo": "Bloated Repo: Detection",
    "relevance_scheduled_builds": "Scheduled Builds: Relevance",
    "detect_scheduled_builds": "Scheduled Builds: Detection",
    "relevance_absent_feedback": "Absent Feedback: Relevance",
    "detect_absent_feedback": "Absent Feedback: Detection",
    "relevance_email_only_notifications": "Email-Only: Relevance",
    "detect_email_only_notifications": "Email-Only: Detection",
    "relevance_skip_failed_tests": "Skip Failed Tests: Relevance",
    "detect_skip_failed_tests": "Skip Failed Tests: Detection",
    "relevance_slow_builds": "Slow Build: Relevance",
    "detect_slow_builds": "Slow Build: Detection",
#
    "ciodor_would_integrate": "I would adopt CI-Odor",
#
    "report_is_useful": "Report is useful/relevant",
    "report_learned_something": "I learned from report",
    "report_made_me_curious": "Report made me curious",
#
    "slowbuild_identification": "Slow Build: Appropriate Identification",
    "slowbuild_actionable": "Slow Build: Know how to address",
    "slowbuild_shouldfail": "Slow Build: Should Fail the Build",
    #
    "failedtests_identification": "Skip Failed Tests: Appropriate Identification",
    "failedtests_actionable": "Skip Failed Tests: Know how to address",
    "failedtests_shouldfail": "Skip Failed Tests: Should Fail the Build",
    #
    "latemerging_identification": "Late Merging: Appropriate Identification",
    "latemerging_actionable": "Late Merging: Know how to address",
    "latemerging_shouldfail": "Late Merging: Should Fail the Build",
    #
    "brokenmaster_awareness_freq": "Broken Rel. Branch: Frequency Awareness",
    "brokenmaster_awareness_timetofix": "Broken Rel. Branch: Time-to-Fix Awareness",
    "brokenmaster_actionable": "Broken Rel. Branch: I know how to improve",
#
    "overall_novelty": "Info is unavailable in other tools",
    "overall_overview": "The reports provide a good overview",
    "overall_positive_effect": "I expect a positive effect",
    "overall_would_integrate": "I would integrate CI-Odor",
}

def to_questions(series):
    return series.map(lambda e: questions[e] if e in questions else e)

likerts = [k for k in df.keys() if k.endswith("_identification") or k.endswith("_actionable") or k.endswith("_shouldfail") or k.startswith("overall_") or k.startswith("brokenmaster_awareness_") or k.startswith("report_")]

df = df[likerts]
print(df[:10])



keys = df.keys()
qs = []

for key in keys:
    q = df.groupby(key)[key]
    q = pd.DataFrame(q.size())[key]
    if "No Answer" in q.index:
        q = q.drop("No Answer")
    q = likert_utils.convert(key, q)
    qs.append(q)

print(qs)







x = pd.DataFrame(qs, columns=["Question", "Count", "L", "SD", "D", "N", "A", "SA", "R"])
#x["Question_Long"] =
x= x.set_index(to_questions(x["Question"]))#,drop=True,inplace=True)

x = x.iloc[::-1]
print(x)

counts = x["Count"]
print(counts)

if "Question" in x.columns:
    print("Dropping column 'Question'")
    x = x.drop(["Question"], axis=1)

if "Count" in x.columns:
    print("Dropping column 'Count'")
    x = x.drop(["Count"], axis=1)

print(x)






# actually do the plotting
likert_colors = ['white', 'firebrick', 'lightcoral', 'gainsboro', 'cornflowerblue', 'darkblue']

a = x.plot.barh(stacked=True, color=likert_colors, figsize=(4, 5), width=.7)
a.set_xlabel("Level of Agreement")
a.autoscale(tight=False)
a.set_xlim([0, 2])

a.figure.patch.set_facecolor('white')

# add proper legend
SD_patch = mpatches.Patch(color='firebrick', label='Strongly Disagree')
D_patch = mpatches.Patch(color='lightcoral', label='Disagree')
N_patch = mpatches.Patch(color='gainsboro', label='Neutral')
A_patch = mpatches.Patch(color='cornflowerblue', label='Agree')
SA_patch = mpatches.Patch(color='darkblue', label='Strongly Agree')
patches = [SD_patch, D_patch, N_patch, A_patch, SA_patch]
a.legend(handles=patches, loc='upper center', frameon=False, ncol=5, bbox_to_anchor=(0.2, 1.1))

delta = 0
for index, row in x.iterrows():
    y = delta - 0.02
    delta += 1

    positive = (row["SA"] + row["A"]) * 100
    neutral = row["N"] * 100
    negative = (row["D"] + row["SD"]) * 100

    a.text(0.02, y, "{:.1f}%".format(negative),
           horizontalalignment='left',
           verticalalignment='center',
           color='lightgray',
           weight='bold',
           bbox=dict(facecolor='none', edgecolor='none'))

    a.text(1, y, "{:.1f}%".format(neutral),
           horizontalalignment='center',
           verticalalignment='center',
           color='gray',
           bbox=dict(facecolor='none', edgecolor='none'))

    a.text(1.98, y, "{:.1f}%".format(positive),
           horizontalalignment='right',
           verticalalignment='center',
           color='lightgray',
           weight='bold',
           bbox=dict(facecolor='none', edgecolor='none'))

    a.text(2.02, y, "{} answers".format(counts[index]),
           horizontalalignment='left',
           verticalalignment='center', )

# a.axes.get_xaxis().set_visible(False)
a.spines['top'].set_visible(False)
a.spines['bottom'].set_visible(False)
plt.tick_params(
    axis='x',  # changes apply to the x-axis
    which='both',  # both major and minor ticks are affected
    bottom=False,  # ticks along the bottom edge are off
    top=False,  # ticks along the top edge are off
    labelbottom=False)  # labels along the bottom edge are off

z = plt.axvline(1, color='black', linewidth=1, linestyle="dotted")
z.set_zorder(-1)

plt.savefig('../agreement-fig8.pdf', bbox_inches="tight")



