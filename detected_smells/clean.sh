#!/bin/bash
echo "Evaluate: google/closure-compiler/master"
cd google_closure-compiler
cat -n skiptests_ciao_google_closure-compiler.csv | sort -uk2 | sort -nk1 | cut -f2- >skiptests_google_closure-compiler.csv
rm skiptests_ciao_google_closure-compiler.csv
cd ..
echo "done"
echo "Evaluate: spring-cloud/spring-cloud-aws/master"
cd spring-cloud_spring-cloud-aws
cat -n skiptests_ciao_spring-cloud_spring-cloud-aws.csv | sort -uk2 | sort -nk1 | cut -f2- >skiptests_spring-cloud_spring-cloud-aws.csv
rm skiptests_ciao_spring-cloud_spring-cloud-aws.csv
cd ..
echo "done"
echo "Evaluate: geotools/geotools/master"
cd geotools_geotools
cat -n skiptests_ciao_geotools_geotools.csv | sort -uk2 | sort -nk1 | cut -f2- >skiptests_geotools_geotools.csv
rm skiptests_ciao_geotools_geotools.csv
cd ..
echo "done"
echo "Evaluate: speedment/speedment/master"
cd speedment_speedment
cat -n skiptests_ciao_speedment_speedment.csv | sort -uk2 | sort -nk1 | cut -f2- >skiptests_speedment_speedment.csv
rm skiptests_ciao_speedment_speedment.csv
cd ..
echo "done"
