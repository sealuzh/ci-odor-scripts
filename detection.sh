#!/bin/bash

rm -r -f detected_smells
java -cp runner.jar ch.uzh.seal.ArtifactRunner compute_smells projects projectsListWithMaster.csv detected_smells/ detected_smells/clean.sh config/config.yaml
cd detected_smells/
sh clean.sh 
cd ..
