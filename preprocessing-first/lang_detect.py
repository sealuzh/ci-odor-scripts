def is_java(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "java" == s or s.startswith("java,") or s.startswith("java ") or s.endswith("java") or "java," in s or "java " in s

def is_scala(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "scala" in s

def is_c(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "c" == s or " c " in s or " c, " in s or s.endswith(" c") or s.startswith("c,") or s.startswith("c ")

def is_cpp(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "c++" in s

def is_csharp(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "c#" in s

def is_javascript(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "javascript" in s or "angular" in s or "node" in s

def is_php(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "php" in s

def is_python(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "python" in s

def is_ruby(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "ruby" in s

def is_go(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "go" in s

def is_bash(s):
    if type(s) is not str:
        return False
    s = s.lower()
    return "bash" in s

for s in [1, 0.1]:
    assert not is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["java", "jAva", ".., java, ..", ".. java ..", ".. Java"]:
    assert is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["scala", "sCala", ".., scala, ..", ".. scala .."]:
    assert not is_java(s)
    assert is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["c", "C", ".., c, ..", ".. c .."]:
    assert not is_java(s)
    assert not is_scala(s)
    assert is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["c++", "C++", ".., c++, ..", ".. c++ .."]:
    assert not is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["c#", "C#", ".., c#, ..", ".. c# .."]:
    assert not is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["javascript", "jaVascrIpt", ".., javascript, ..", ".. javascript ..", "angular", "angular.js", "angularjs", "node", "nodejs", "node.js"]:
    assert not is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["php", "pHp", ".., php, ..", ".. php .."]:
    assert not is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["python", "pYthon", ".., python, ..", ".. python .."]:
    assert not is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["ruby", "rUby", ".., ruby, ..", ".. ruby .."]:
    assert not is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert is_ruby(s)
    assert not is_go(s)
    assert not is_bash(s)

for s in ["go", "gO", ".., go, ..", ".. go ..", "golang"]:
    assert not is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert is_go(s)
    assert not is_bash(s)

for s in ["bash", "bAsh", ".., bash, ..", ".. bash .."]:
    assert not is_java(s)
    assert not is_scala(s)
    assert not is_c(s)
    assert not is_cpp(s)
    assert not is_csharp(s)
    assert not is_javascript(s)
    assert not is_php(s)
    assert not is_python(s)
    assert not is_ruby(s)
    assert not is_go(s)
    assert is_bash(s)



