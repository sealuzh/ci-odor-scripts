import pandas as pd

renames = {
    # id
    "Response ID": "id",
    # demographics
    "What is your highest qualification related to Computer Science?": "demographics_education",
    "Other:What is your highest qualification related to Computer Science?": "demographics_education_other",
    "What is your current position?": "demographics_position",
    "Other:What is your current position?": "demographics_position_other",
    "In which domain do you typically create software (e.g., financial, game, etc.)?": "demographics_domain",
    "How would you rate your general programming experience?": "demographics_experience_general",
    "How would you rate your knowledge about CI principles?": "demographics_experience_ci_theory",
    "How would you rate your experience using and maintaining a CI pipeline?": "demographics_experience_ci_practice",
    "In which role do you typically get in contact with CI (e.g., as developer, tester, product owner, etc.)?": "demographics_role",
    "In which programming languages do you typically use a CI process?": "pl_open_question",
    "Maven:Which build tools do you typically use? (optional)": "demographics_ci_tool_maven",
    "Gradle:Which build tools do you typically use? (optional)": "demographics_ci_tool_gradle",
    "Ant:Which build tools do you typically use? (optional)": "demographics_ci_tool_ant",
    "Others:Which build tools do you typically use? (optional)": "demographics_ci_tool_others_question",
    "Others:Which build tools do you typically use? (optional).1": "demographics_ci_tool_others",
    # problem statement
    "CI best-practices are no strict rules, they can be adapted for a project. Do you agree?": "problem_relaxed_principles",
    "One can deviate from CI best-practices unintentionally. Do you agree?": "problem_unintentional_deviation",
    "The benefit of using CI diminishes, the more deviations from CI best-practices exist. Do you agree?": "problem_ci_decay",
    "In your experience, what is the effect of deviating from CI best-practices? (Optional)": "problem_decay_effect",
    # late_merging
    "This anti-pattern is relevant in a typical CI pipeline. Do you agree?": "relevance_late_merging",
    "The detection strategy is sufficient to identify occurrences of this anti-pattern. Do you agree?": "detect_late_merging",
    "Do you have any comments? How would you improve the detection strategy? (optional)": "comments_late_merging",
    # aged_branches
    "This anti-pattern is relevant in a typical CI pipeline. Do you agree?.1": "relevance_aged_branches",
    "The detection strategy is sufficient to identify occurrences of this anti-pattern. Do you agree?.1": "detect_aged_branches",
    "Do you have any comments? How would you improve the detection strategy? (optional).1": "comments_aged_branches",
    # broken_master
    "This anti-pattern is relevant in a typical CI pipeline. Do you agree?.2": "relevance_broken_master",
    "The detection strategy is sufficient to identify occurrences of this anti-pattern. Do you agree?.2": "detect_broken_master",
    "Do you have any comments? How would you improve the detection strategy? (optional).2": "comments_broken_master",
    # bloated_repo
    "This anti-pattern is relevant in a typical CI pipeline. Do you agree?.3": "relevance_bloated_repo",
    "The detection strategy is sufficient to identify occurrences of this anti-pattern. Do you agree?.3": "detect_bloated_repo",
    "Do you have any comments? How would you improve the detection strategy? (optional).3": "comments_bloated_repo",
    # scheduled_builds
    "This anti-pattern is relevant in a typical CI pipeline. Do you agree?.4": "relevance_scheduled_builds",
    "The detection strategy is sufficient to identify occurrences of this anti-pattern. Do you agree?.4": "detect_scheduled_builds",
    "Do you have any comments? How would you improve the detection strategy? (optional).4": "comments_scheduled_builds",
    # absent_feedback
    "This anti-pattern is relevant in a typical CI pipeline. Do you agree?.5": "relevance_absent_feedback",
    "The detection strategy is sufficient to identify occurrences of this anti-pattern. Do you agree?.5": "detect_absent_feedback",
    "Do you have any comments? How would you improve the detection strategy? (optional).5": "comments_absent_feedback",
    # email_only_notifications
    "This anti-pattern is relevant in a typical CI pipeline. Do you agree?.6": "relevance_email_only_notifications",
    "The detection strategy is sufficient to identify occurrences of this anti-pattern. Do you agree?.6": "detect_email_only_notifications",
    "Do you have any comments? How would you improve the detection strategy? (optional).6": "comments_email_only_notifications",
    # skip_failed_tests
    "This anti-pattern is relevant in a typical CI pipeline. Do you agree?.7": "relevance_skip_failed_tests",
    "The detection strategy is sufficient to identify occurrences of this anti-pattern. Do you agree?.7": "detect_skip_failed_tests",
    "Do you have any comments? How would you improve the detection strategy? (optional).7": "comments_skip_failed_tests",
    # slow_builds
    "This anti-pattern is relevant in a typical CI pipeline. Do you agree?.8": "relevance_slow_builds",
    "The detection strategy is sufficient to identify occurrences of this anti-pattern. Do you agree?.8": "detect_slow_builds",
    "Do you have any comments? How would you improve the detection strategy? (optional).8": "comments_slow_builds",
    # result
    "I would integrate such a tool in my CI pipeline. Do you agree?": "ciodor_would_integrate",
    "What do you expect would be the effect of integrating such a tool in a CI pipeline?": "ciodor_effect",
    #  outro
    "Do you have any further comments regarding this questionnaire or our research in general?": "outro_further_comments",
    "Name:Please consider filling out the following optional fields..1": "name",
    "Email:Please consider filling out the following optional fields..1": "email",
    "You can contact me again in the context of this study, e.g., to inform me about the results or for clarification questions.:Please consider filling out the following optional fields.": "can_contact",
    "I would like to participate in the raffle of the two vouchers.:Please consider filling out the following optional fields.": "wants_raffle",
}

drops = [
    "Time Started",
    "Date Submitted",
    "Referer",
    "Extended Referer",
    "SessionID",
    "User Agent",
    "Extended User Agent",
    "Contact ID",
    "Legacy Comments",
    "Comments",
    "Tags",
    "Language",
    "IP Address",
    "Longitude",
    "Latitude",
    "Country",
    "City",
    "State/Region",
    "Postal",
]

questions = {}

for key in renames:
    questions[renames[key]] = key



def readRawSurvey(csv_file):
    print ("reading survey from " + csv_file)
    df = pd.read_csv(csv_file)
    df = df.rename(index=str, columns=renames)
    df = df.drop(drops, axis=1)

    numTotal = df["id"].count()
    numComplete = df[df["Status" ] == "Complete"]["id"].count()
    numPseudoComplete = df[df["detect_slow_builds" ] != ""]["id"].count()

    print ("found {} raw responses ({} complete, {} pseudo complete)".format(numTotal, numComplete, numPseudoComplete))

    return df

def readSurvey(csv_file = "survey-preprocessed.csv"):
    print ("reading preprocessed survey from " + csv_file)
    df = pd.read_csv(csv_file, index_col=0)
    numTotal = df["id"].count()
    print("contains {} responses".format(numTotal))
    return df
