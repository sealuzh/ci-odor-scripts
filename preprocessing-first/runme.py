import preprocessing
import matplotlib.pyplot as plt
import pandas as pd
import lang_detect

# read raaw data
df = preprocessing.readRawSurvey("survey-180802.csv")

# analyze open anser to identify languages
df['pl_java'] = df["pl_open_question"].map(lang_detect.is_java)
df['pl_scala'] = df["pl_open_question"].map(lang_detect.is_scala)
df['pl_c'] = df["pl_open_question"].map(lang_detect.is_c)
df['pl_cpp'] = df["pl_open_question"].map(lang_detect.is_cpp)
df['pl_csharp'] = df["pl_open_question"].map(lang_detect.is_csharp)
df['pl_js'] = df["pl_open_question"].map(lang_detect.is_javascript)
df['pl_php'] = df["pl_open_question"].map(lang_detect.is_php)
df['pl_python'] = df["pl_open_question"].map(lang_detect.is_python)
df['pl_ruby'] = df["pl_open_question"].map(lang_detect.is_ruby)
df['pl_go'] = df["pl_open_question"].map(lang_detect.is_go)
df['pl_bash'] = df["pl_open_question"].map(lang_detect.is_bash)

# debug transformation
pls = df[["pl_open_question", "pl_java", "pl_scala", "pl_c", "pl_cpp", "pl_csharp", "pl_js", "pl_php", "pl_python", "pl_ruby", "pl_go", "pl_bash"]]
print(pls[:15])
pls = pls.drop(columns=["pl_open_question"])
pls[:10]

# create statistic about mentioned languages
pl_counts = pls.sum().sort_values(ascending=False)
print(type(pl_counts))
a = pl_counts.plot(kind="bar")
a.set_xlabel("Programming Language")
a.set_ylabel("x-Times Mentioned in Answer")
plt.savefig('mentioned-languages.pdf', bbox_inches="tight")

# create statistic about CI experience
demo = df[["demographics_education","demographics_position","demographics_experience_general","demographics_experience_ci_theory","demographics_experience_ci_practice"]]
print(demo[:10])

plt.figure()
x = demo[["demographics_experience_ci_theory","demographics_experience_ci_practice"]]
x = x.groupby(["demographics_experience_ci_theory","demographics_experience_ci_practice"])["demographics_experience_ci_theory"].count()
x.plot(kind='barh', stacked=True)
plt.savefig('ci-experience.pdf', bbox_inches="tight")

# more specific statistics about CI experience
def printHighToLow_field(df, key):
    x = df.groupby(key)[key].count()
    x = pd.DataFrame(x).transpose()

    print(x)
    print("Total: " + str(x["High"] + x["Moderate"] + x["Low"] + x["No Answer"]))
    x = x[["High", "Moderate", "Low", "No Answer"]]

    plt.figure()
    a = x.plot(kind = "barh", stacked=True, figsize=(8,.5))
    a.legend(loc='upper center', frameon=False, ncol=7,bbox_to_anchor=(0.5,2))
    a.autoscale(tight=True)
    plt.savefig('high-to-low-'+key+'.pdf', bbox_inches="tight")

printHighToLow_field(demo, "demographics_experience_general")
printHighToLow_field(demo, "demographics_experience_ci_theory")
printHighToLow_field(demo, "demographics_experience_ci_practice")

print("after import: {}".format(len(df.index)))

keys = [
    "demographics_experience_general",
    "demographics_experience_ci_theory",
    "demographics_experience_ci_practice"]

# dropping sensitive fields for artifact
tmp = df.drop(["Name:Please consider filling out the following optional fields.",
               "Email:Please consider filling out the following optional fields.", "wants_raffle",
               "can_contact", "name", "email"], axis=1)

for key in keys:
    tmp = tmp.loc[df[key].isin(["High", "Moderate"])]
    print("only keeping records with '{}' on 'high' and 'moderate': {}".format(key, len(tmp.index)))

tmp.to_csv("../survey-preprocessed.csv")