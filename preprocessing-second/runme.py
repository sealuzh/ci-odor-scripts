import pandas as pd
import preprocessing2 as pp
df = pp.readRawSurvey("survey2-180822.csv")

# Disqualifying errorneous answers in first part...
for idx in [88, 93]:
    df.at[idx,'has_seen_report'] = "No"
    df.at[idx,'report_project'] = pd.np.nan
    df.at[idx,'report_is_useful'] = pd.np.nan
    df.at[idx,'report_learned_something'] = pd.np.nan
    df.at[idx,'report_made_me_curious'] = pd.np.nan
    df.at[idx,'report_thoughts'] = pd.np.nan


# simple statistic about original devs
print("### original devs ###")
origs = df[(df["has_seen_report"] == "Yes")]
print(origs.index)

print("\n### keys ###")
print(df.keys())

# set completion status of questionaire
def fun_set_status(row):
    missesQuestion = pd.isna(row["has_seen_report"])
    missesLastSpecific = pd.isna(row["report_made_me_curious"])
    missesLastGeneral = pd.isna(row["overall_would_integrate"])

    if missesQuestion:
        return "Incomplete"
    else:
        if not missesLastSpecific and missesLastGeneral:
            return "Completed Only Specific"
        if missesLastSpecific and not missesLastGeneral:
            return "Completed Only General"
        if not missesLastSpecific and not missesLastGeneral:
            return "Completed Both"

    return "Partial"

df["Status"] = df.apply(fun_set_status, axis=1)

# simple statistic about completion status
g = df.groupby("Status")["Status"].count()
total = g.sum()
completion_rate = (g["Completed Both"] + g["Completed Only General"]) / total

print(g)
print("Total: {}".format(total))
print ("Completion Rate: ('compl. both' + 'compl. only general') / total = {:.1f}%".format(100*completion_rate))




# fill skipped questions with "No Answer"
default_values = [
    "report_is_useful",
    "report_learned_something",
    "report_made_me_curious",
    #"report_project": "",
    #"report_thoughts": "",
    # slow build
    "slowbuild_identification",
    "slowbuild_actionable",
    "slowbuild_shouldfail",
    #"slowbuild_thoughts",
    # failed tests
    "failedtests_identification",
    "failedtests_actionable",
    "failedtests_shouldfail",
    #"failedtests_thoughts",
    # late merging
    "latemerging_identification",
    "latemerging_actionable",
    "latemerging_shouldfail",
    #"latemerging_thoughts",
    # broken release branch
    "brokenmaster_awareness_freq",
    "brokenmaster_awareness_timetofix",
    "brokenmaster_actionable",
    #"brokenmaster_thoughts",
    # overall
    "overall_novelty",
    "overall_overview",
    "overall_positive_effect",
    "overall_would_integrate",
    #"overall_thoughts",
    # demographics
    "demographics_education",
    #"demographics_degree_other",
    "demographics_position",
    #"demographics_position_other",
    #"demographics_domain",
    "demographics_experience_programming",
    "demographics_ci_theory",
    "demographics_ci_practice",
    "demographics_role",
    #"name",
    #"email",
    #"include_in_raffle",
    #"can_contact",
    #"questionnaire_thoughts",
]

#df["has_seen_report"] = df["has_seen_report"].fillna("No")
for key in default_values:
    df[key] = df[key].fillna("No Answer")

print(df[:6])




# filter invalid records and export to .csv
print("number of records after import: {}".format(len(df.index)))

keys = [
    "demographics_experience_programming",
    "demographics_ci_theory",
    "demographics_ci_practice"]

# dropping sensitive fields for artifact
tmp = df.drop(["include_in_raffle", "name", "email", "can_contact"], axis=1)


tmp = tmp.loc[-tmp["Status"].isin(["Incomplete"])]
print("only keeping records with 'Status' is not 'Incomplete': {}".format(len(tmp.index)))

for key in keys:
    tmp = tmp.loc[~tmp[key].isin(["None", "Low"])] # keeping 'high', 'moderate', and 'No Answer'!
    print("only keeping records with '{}' is not 'None' and 'Low': {}".format(key, len(tmp.index)))

tmp.to_csv("../survey2-preprocessed.csv")
