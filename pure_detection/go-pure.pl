#! /usr/bin/perl

$ARGV[0] || die "Syntax: $0 OUTDIR <list";
$outdir=$ARGV[0];

@lines=<STDIN>;


foreach $l(@lines)
  {
    chomp($l);
    print "ANALYZING $l...\n";
    system("rm -f *.csv");
    system("cp $l/*.csv .");
    
    system("mkdir $outdir/$l");
    $string="R -e \"rmarkdown::render('analyzer.R',params=list(project= '";
    $string.=$l;
    $string.="'),output_file='";
    $string.="index.html')\"";

    #print "$string\n";
    system("$string");    
  }
system("rm -f *.csv");

