You need:

R 3.4.1 or greater, with the following packages installed:
knitr
dplyr
kableExtra
htmltools
lubridate


RStudio 1.1.4 or greater
Perl interpreter
*nix shell

when running run.sh, you need to set the RSTUDIO_PANDOC environment variable

Furthermore, it is needed to update the cleaned smell computations of the projects in the root folder and update list file.

### skip tests and late merging are not updated

