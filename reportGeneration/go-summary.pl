#! /usr/bin/perl

$ARGV[0] || die "Syntax: $0 OUTDIR <list";
$outdir=$ARGV[0];

@lines=<STDIN>;



foreach $l(@lines)
  {
    system("rm -rf figure");
    chomp($l);
    print "ANALYZING $l...\n";
    system("rm -f *.csv");
    system("cp $l/*.csv .");
    
    system("mkdir $outdir/$l");
    $string="R -e \"rmarkdown::render('summarizer.R',params=list(project= '";
    $string.=$l;
    $string.="'),output_file='";
    $string.=$l.".html')\"";

    #print "$string\n";
    system("$string");
    system("mv figure $outdir/$l");
    system("mv $l".".html $outdir/$l");
    system("cp cisummary.css $outdir/$l");
    
  }
system("rm -f *.csv");

